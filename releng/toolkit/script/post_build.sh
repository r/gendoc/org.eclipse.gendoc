#!/bin/bash -x
profile=$1
p2UpdateSite=$WORKSPACE/releng/org.eclipse.gendoc.update-site/target
updateSite=$WORKSPACE
downloadArea=/home/data/httpd/download.eclipse.org/gendoc
nightlyDownloadTarget=$downloadArea/updates/nightly/latest/$profile
nightlyDropsTarget=$downloadArea/nightly/latest/$profile
timestamp=$(date +%s000)

rm -rf $updateSite/repository
rm -rf $updateSite/*.zip
mkdir -p $updateSite/repository
mv $p2UpdateSite/repository $updateSite
mv $p2UpdateSite/*.zip $updateSite

ssh genie.gendoc@projects-storage.eclipse.org rm -rf $nightlyDownloadTarget
ssh genie.gendoc@projects-storage.eclipse.org mkdir -p $nightlyDownloadTarget
scp -r  $updateSite/repository/* genie.gendoc@projects-storage.eclipse.org:$nightlyDownloadTarget/

ssh genie.gendoc@projects-storage.eclipse.org rm -rf $nightlyDropsTarget
ssh genie.gendoc@projects-storage.eclipse.org mkdir -p $nightlyDropsTarget
scp -r $updateSite/*.zip genie.gendoc@projects-storage.eclipse.org:$nightlyDropsTarget/

scp -r  $WORKSPACE/releng/toolkit/downloads/updates/nightly/latest/*.xml genie.gendoc@projects-storage.eclipse.org:$nightlyDownloadTarget/../
scp -r  $WORKSPACE/releng/toolkit/downloads/updates/nightly/latest/p2.index genie.gendoc@projects-storage.eclipse.org:$nightlyDownloadTarget/../
