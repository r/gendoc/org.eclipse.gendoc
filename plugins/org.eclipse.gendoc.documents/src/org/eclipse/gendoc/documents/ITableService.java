/*****************************************************************************
 * Copyright (c) 2017 Atos.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Mohamed Ali Bach Tobji (Atos) mohamed-ali.bachtobji@atos.net - Initial API and implementation
 *****************************************************************************/

package org.eclipse.gendoc.documents;

import org.eclipse.gendoc.services.IService;
import org.eclipse.gendoc.services.exception.InvalidContentException;
import org.eclipse.gendoc.tags.ITag;
import org.w3c.dom.Node;

import org.eclipse.gendoc.table.Table;

public interface ITableService extends IService {
	
	
	/**
	 * Returns the generated table
	 * @param tag
	 * @param documentService
	 * @param returnValue
	 * @param tableModel
	 * @param tableNode
	 * @return
	 * @throws InvalidContentException
	 */
	String manageTable(ITag tag, IDocumentService documentService, StringBuffer returnValue, Object tableModel,
			Node tableNode) throws InvalidContentException;
	
	/**
	 * Returns a unique Id for the table parameter
	 * @param e
	 * @return
	 */
	String getTableId(Table e);
	
	
	/**
	 * Returns the table whose id is given as parameter  
	 * @param tableId
	 * @return
	 */
	Table getTable(String tableId);

}
