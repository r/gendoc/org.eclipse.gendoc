/*****************************************************************************
 * Copyright (c) 2017 Atos.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Mohamed Ali Bach Tobji (Atos) mohamed-ali.bachtobji@atos.net - Initial API and implementation
 *****************************************************************************/

package org.eclipse.gendoc.documents;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.gendoc.documents.impl.IDGenerator;
import org.eclipse.gendoc.services.AbstractService;

import org.eclipse.gendoc.table.Table;

public abstract class AbstractTableService extends AbstractService implements ITableService{

	/**
	 * 
	 * This table is dedicated to save table instances
	 * Table Map 
	 */
	private final Map<String, Table> tableMap = new HashMap<String,Table>();
	
	public String getTableId(Table table) {
		String key = IDGenerator.nextID();
		tableMap.put(key, table);
		return key;
	}
	
	public Table getTable(String tableId){
		return tableMap.get(tableId);
	}
}
