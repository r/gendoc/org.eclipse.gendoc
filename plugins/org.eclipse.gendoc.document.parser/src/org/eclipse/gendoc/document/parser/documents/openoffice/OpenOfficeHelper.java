/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.documents.openoffice;

/**
 * The Class XLSXHelper.
 */
public class OpenOfficeHelper
{    
	public static final String GENDOC_NS = "http://gendoc.eclipse.org/main";

	// Content Type(s):
    // Drawing
    public static final String DRAWING_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.drawing+xml";
    public static final String DRAWING_RELATIONSHIP = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing";
    
    public static final String DRAWING_ML_NAMESPACE = "http://schemas.openxmlformats.org/drawingml/2006/main";
    public static final String RELATIONSHIPS_NAMESPACE = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
    public static final String PACKAGE_RELATIONSHIPS_NAMESPACE = "http://schemas.openxmlformats.org/package/2006/relationships";
    public static final String CONTENT_TYPES_NAMESPACE = "http://schemas.openxmlformats.org/package/2006/content-types";
    public static final String MARKUP_COMPATIBILITY_NAMESPACE = "http://schemas.openxmlformats.org/markup-compatibility/2006";
}
