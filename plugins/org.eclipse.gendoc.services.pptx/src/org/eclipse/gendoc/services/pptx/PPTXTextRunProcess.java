package org.eclipse.gendoc.services.pptx;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gendoc.document.parser.documents.Document;
import org.eclipse.gendoc.document.parser.documents.helper.XMLHelper;
import org.eclipse.gendoc.document.parser.pptx.PPTXHelper;
import org.eclipse.gendoc.documents.IDocumentService;
import org.eclipse.gendoc.process.AbstractStepProcess;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.exception.GenDocException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PPTXTextRunProcess extends AbstractStepProcess {
	public PPTXTextRunProcess() {
	}

	@Override
	protected void doRun() throws GenDocException {
		IDocumentService docService = GendocServices.getDefault().getService(IDocumentService.class);
		if (!(docService instanceof PPTXDocumentService))
			return;
		super.doRun();
	}

	@Override
	protected void step(Document document) throws GenDocException {
		Node currentNode = document.getXMLParser().getCurrentNode();
		List<Node> nodesToDelete = new ArrayList<Node>();
		if (currentNode.getNodeName().equals(PPTXHelper.DRAWING_ML_TEXT_PARAGRAPH)) {
			nodesToDelete.clear();
			Element el = (Element)currentNode;
			NodeList nl = el.getChildNodes();
			Element lastText = null; 
			String lastRunProps = null;
			for (int i=0; i<nl.getLength(); i++) {
				if (!(nl.item(i) instanceof Element))
					continue;
				
				Element elem = (Element)nl.item(i);
				if (elem.getNodeName().equals("a:r")) {
					// Text Run element
					Element text = (Element)elem.getElementsByTagName("a:t").item(0);				
					NodeList nl2 = elem.getElementsByTagName("a:rPr");				
					Element rPr = (Element)(nl2.getLength() > 0 ? nl2.item(0) : null);
					String rPrTxt = "";
					if (rPr != null) {
						rPr.removeAttribute("lang");
						rPr.removeAttribute("dirty");
						rPr.removeAttribute("err");
						rPrTxt = XMLHelper.asText(rPr, false).trim();
					}
					if (lastText == null) {
						lastRunProps = rPrTxt;
						lastText = text;
						continue;
					}
					
					if (rPrTxt.equals(lastRunProps)) {
						// Same kind of run...
						lastText.setTextContent(lastText.getTextContent()+text.getTextContent());
						nodesToDelete.add(elem);
					}
				} else {
					lastText = null;
					lastRunProps = null;
				}
			}
			
			for (Node n : nodesToDelete) {
				n.getParentNode().removeChild(n);
			}
		}
		
	}

}
