package org.eclipse.gendoc.services.pptx.tags;

import java.util.LinkedList;
import java.util.regex.Pattern;

import org.eclipse.gendoc.document.parser.documents.Document;
import org.eclipse.gendoc.document.parser.documents.Document.PROPERTY;
import org.eclipse.gendoc.document.parser.pptx.PPTXHelper;
import org.eclipse.gendoc.documents.IDocumentService;
import org.eclipse.gendoc.process.AbstractStepProcess;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.exception.GenDocException;
import org.eclipse.gendoc.services.pptx.PPTXDocumentService;
import org.eclipse.gendoc.services.pptx.PPTXRegisteredTags;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class SlideDropProcess  extends AbstractStepProcess {
	protected static final Pattern DROP_SLIDE_TAG_PATTERN = Pattern.compile("<\\s*" + PPTXRegisteredTags.DROP_SLIDE + "\\s*/\\s*>", Pattern.MULTILINE);
	
	protected IDocumentService documentService = GendocServices.getDefault().getService(IDocumentService.class);
	
	protected LinkedList<Node> nodesToRemove;

	public SlideDropProcess() {
	}


	@Override
	protected void doRun() throws GenDocException {
		if (!(documentService instanceof PPTXDocumentService))
			return;
		
		nodesToRemove = new LinkedList<Node>();

		super.doRun();

		for (Node n : nodesToRemove) {
			Node parent = n.getParentNode();
			if (parent != null) {
				parent.removeChild(n);
			}
		}
	}

	@Override
	protected void step(Document document) throws GenDocException
	{
		Node currentNode = document.getXMLParser().getCurrentNode();
		if (documentService.isPara(currentNode.getNodeName())) {
			String text = (String)document.get(PROPERTY.text);
			if (text != null && DROP_SLIDE_TAG_PATTERN.matcher(text).find()) {
				Node start = findPrevStartSlideNode(currentNode);
				Node end = findNextEndSlideNode(currentNode);
				if (start == null || end == null) 
					return;
				for (Node n = start; n != end; n = n.getNextSibling())
					nodesToRemove.add(n);
				nodesToRemove.add(end);
			}
		}
	}
	
	private Node findPrevStartSlideNode(Node n) {
		return findSlideRefNode(n,true,PPTXHelper.GENDOC_REF_TYPE.startSlide);
	}
	
	
	private Node findNextEndSlideNode(Node n) {
		return findSlideRefNode(n,false,PPTXHelper.GENDOC_REF_TYPE.endSlide);
	}
		
	private Node findSlideRefNode(Node n, boolean previous, PPTXHelper.GENDOC_REF_TYPE refType) {
		Node sibling = findFlattenStructureChildNode(n);
		if (sibling == null)
			return null;
		
		while (sibling != null) {
			if (sibling.getNodeName().equals(PPTXHelper.GENDOC_REF_ELEM)) {
				String typeStr = ((Element)sibling).getAttribute(PPTXHelper.GENDOC_REF_ELEM_TYPE_ATTR);
				PPTXHelper.GENDOC_REF_TYPE t = PPTXHelper.GENDOC_REF_TYPE.valueOf(typeStr);
				if (t == refType)
					return sibling;
			}
			sibling = previous ? sibling.getPreviousSibling() : sibling.getNextSibling(); 
		}
		return null;
	}

	private Node findFlattenStructureChildNode(Node n) {
		while (n != null) {
			if (n.getParentNode() != null && n.getParentNode().getNodeName().equals(PPTXHelper.GENDOC_FLAT_STRUCTURE_ELEM))
				return n;
			n = n.getParentNode();
		}
		return null;
	}			
}
