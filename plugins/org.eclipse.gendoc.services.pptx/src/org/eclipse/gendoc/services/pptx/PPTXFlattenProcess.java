package org.eclipse.gendoc.services.pptx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.xpath.XPathExpressionException;

import org.eclipse.gendoc.document.parser.documents.Document;
import org.eclipse.gendoc.document.parser.documents.XMLParser;
import org.eclipse.gendoc.document.parser.documents.openoffice.OpenOfficeDocument;
import org.eclipse.gendoc.document.parser.pptx.PPTXDocument;
import org.eclipse.gendoc.document.parser.pptx.PPTXHelper;
import org.eclipse.gendoc.document.parser.pptx.PPTXParser;
import org.eclipse.gendoc.document.parser.pptx.PPTXRef;
import org.eclipse.gendoc.document.parser.pptx.XPathPptxUtils;
import org.eclipse.gendoc.documents.IDocumentManager;
import org.eclipse.gendoc.documents.IDocumentService;
import org.eclipse.gendoc.process.AbstractProcess;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.exception.GenDocException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PPTXFlattenProcess extends AbstractProcess {

	@Override
	protected void doRun() throws GenDocException {
        IDocumentService docService = GendocServices.getDefault().getService(IDocumentService.class);
		if (!(docService instanceof PPTXDocumentService))
			return;

		final IDocumentManager documentManager = GendocServices.getDefault().getService(IDocumentManager.class);
        Document document = documentManager.getDocTemplate();
        document.jumpToStart();

		PPTXParser parser = (PPTXParser)document.getXMLParser();
		Element gendocExt = (Element)parser.getDocument().getDocumentElement().appendChild(
				parser.getDocument().createElementNS(PPTXHelper.GENDOC_NS,PPTXHelper.GENDOC_FLAT_STRUCTURE_ELEM));
		gendocExt.setAttribute("xmlns:gendoc", PPTXHelper.GENDOC_NS);
		do {
	        if (PPTXHelper.SLIDE_ID_NODE.equals(parser.getCurrentNode().getNodeName()))
	        {
	            Node item = parser.getCurrentNode().getAttributes().getNamedItem(PPTXHelper.SLIDE_ID_ATTR);
	            if (item != null)
	            {
	            	String relId = item.getNodeValue();
	            	try {
						XMLParser slideParser = parser.loadExplicitPartDocument(PPTXHelper.SLIDE_RELATIONSHIP, relId);
						createSlideFlattenStructure((OpenOfficeDocument)document,slideParser,gendocExt);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}                            	
	            }
	        }
		} while (document.next());
	}

	protected void createSlideFlattenStructure(OpenOfficeDocument document, XMLParser parser, Element ownerEl) {		
		org.w3c.dom.Document doc = ownerEl.getOwnerDocument();
		
		PPTXRef slideRef = new PPTXRef(document.getRelativePath(parser),
				XPathPptxUtils.INSTANCE.getNodeXPath(doc.getDocumentElement()));

		StringBuffer paragRefs = new StringBuffer();
		Element startSlide = (Element)ownerEl.appendChild(doc.createElement(PPTXHelper.GENDOC_REF_ELEM));
		startSlide.setAttribute(PPTXHelper.GENDOC_REF_ELEM_TYPE_ATTR, 
				PPTXHelper.GENDOC_REF_TYPE.startSlide.name());
		startSlide.setAttribute(PPTXHelper.GENDOC_REF_ELEM_REF_ATTR,slideRef.toString());
		
		try {
			NodeList parasNodeList = XPathPptxUtils.INSTANCE.evaluateNodes(parser.getDocument(), "//"+PPTXHelper.DRAWING_ML_TEXT_PARAGRAPH);
			ArrayList<Node> paras = new ArrayList<Node>();
			for (int i=0;i<parasNodeList.getLength(); i++)
				paras.add(parasNodeList.item(i));
			Collections.sort(paras, new TextBoxNodeComparator((PPTXDocument)document, parser));
			for (int i=0;i<paras.size(); i++) {
				Element n = (Element)paras.get(i);
				String xpathToEl = XPathPptxUtils.INSTANCE.getNodeXPath(n);
				n = (Element)doc.importNode(n, true);
				String ref = new PPTXRef(xpathToEl).toString();
				n.setAttributeNS(PPTXHelper.GENDOC_NS, PPTXHelper.GENDOC_XPATHREF_ATTR, ref);
				if (paragRefs.length() != 0)
					paragRefs.append(",");
				paragRefs.append(ref);
				ownerEl.appendChild(n);
			}
		} catch (XPathExpressionException e) {}

		Element endSlide = (Element)ownerEl.appendChild(doc.createElement(PPTXHelper.GENDOC_REF_ELEM));
		endSlide.setAttribute(PPTXHelper.GENDOC_REF_ELEM_TYPE_ATTR, 
				PPTXHelper.GENDOC_REF_TYPE.endSlide.name());
		endSlide.setAttribute(PPTXHelper.GENDOC_REF_ELEM_REF_ATTR, slideRef.toString());
		
		startSlide.setAttribute("paras", paragRefs.toString());
	}

	@Override
	protected int getTotalWork() {
		return 1;
	}

	private static class TextBoxNodeComparator implements Comparator<Node> {
		public TextBoxNodeComparator(PPTXDocument doc, XMLParser slideParser) {
			this.slideParser = slideParser;
			this.pptxDocument = doc;
		}
		
		@Override
		public int compare(Node o1, Node o2) {
			try {
				Node xfrm1 = getXFrameNode(o1);
				Node xfrm2 = getXFrameNode(o2);
				
				if (xfrm1 == null && xfrm2 != null) {
					return Integer.MAX_VALUE;
				} else if (xfrm2 == null && xfrm1 != null)
					return Integer.MIN_VALUE;
				
				long y1 = XPathPptxUtils.INSTANCE.evaluateNumber(xfrm1,"./a:off/@y",Integer.MAX_VALUE);
				long y2 = XPathPptxUtils.INSTANCE.evaluateNumber(xfrm2,"./a:off/@y",Integer.MAX_VALUE);
				
				if (Math.abs(y1-y2) < 100) {
					long x1 = XPathPptxUtils.INSTANCE.evaluateNumber(xfrm1,"./a:off/@x",Integer.MAX_VALUE);
					long x2 = XPathPptxUtils.INSTANCE.evaluateNumber(xfrm2,"./a:off/@x",Integer.MAX_VALUE);
					if (Math.abs(x1-x2) < 100) {
						return 0;
					}
					return (int)(x1-x2);				
				} else 
					return (int)(y1 - y2);
			} catch (XPathExpressionException e) {
				return 0;
			}
				
		}
		
		private Node getXFrameNode(Node par) throws XPathExpressionException {
			
			Element xfrm = (Element)XPathPptxUtils.INSTANCE.evaluateNode(par,"./ancestor::p:sp/p:spPr/a:xfrm");
			if (xfrm == null) {
				Node phNode = XPathPptxUtils.INSTANCE.evaluateNode(par,"./ancestor::p:sp/p:nvSpPr/p:nvPr/p:ph");
				if (phNode == null)
					return null;
				String idx = XPathPptxUtils.INSTANCE.evaluateText(phNode,"./@idx");
				String type = XPathPptxUtils.INSTANCE.evaluateText(phNode,"./@type");
				String relsPath = pptxDocument.getRelativePath(slideParser);
				relsPath = relsPath.replace("/slides/","/slides/_rels/")+".rels";
				XMLParser slideRelsPath = pptxDocument.getSubdocument(relsPath);
				String layoutFile = XPathPptxUtils.INSTANCE.evaluateText(slideRelsPath.getDocument(),
						"//rel:Relationship[@Type='http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout']/@Target");
				layoutFile = "/ppt/slideLayouts/"+layoutFile.substring(layoutFile.lastIndexOf('/')+1);
				XMLParser slideLayoutParser = pptxDocument.getSubdocument(layoutFile);
				
				xfrm = (Element)XPathPptxUtils.INSTANCE.evaluateNode(slideLayoutParser.getDocument(),"//p:sp[p:nvSpPr/p:nvPr/p:ph[@idx='"+idx+"']]/p:spPr/a:xfrm");
				if (xfrm == null)
					xfrm = (Element)XPathPptxUtils.INSTANCE.evaluateNode(slideLayoutParser.getDocument(),"//p:sp[p:nvSpPr/p:nvPr/p:ph[@type='"+type+"']]/p:spPr/a:xfrm");					
			}
	
			return xfrm;
		}
		
		private PPTXDocument pptxDocument;
		private XMLParser slideParser;
	}
	
}
