/*****************************************************************************
 * Copyright (c) 2010 Atos Origin.
 * Copyright (c) 2011 Airbus.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Caroline Bourdeu d'Aguerre (Atos Origin) caroline.bourdeudaguerre@atosorigin.com - Initial API and implementation
 *  Pierre Gaufillet (Airbus) pierre.gaufillet@airbus.com - remove modal box after successful generations
 *  Antonio Campesino Robles (Ericsson) - Bug 501157
 *
 *****************************************************************************/
package org.eclipse.gendoc.ui.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.gendoc.ui.Activator;
import org.eclipse.gendoc.ui.run.GenDocRunnable;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Our sample action implements workbench action delegate. The action proxy will be created by the workbench and shown
 * in the UI. When the user tries to use the action, this delegate will be created and execution will be delegated to
 * it.
 * 
 * @see IWorkbenchWindowActionDelegate
 */
public class GenerateDocumentation extends AbstractHandler
{

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection s = HandlerUtil.getActiveWorkbenchWindow(event)
	                .getActivePage().getSelection();
		IStructuredSelection selection = null;
		if (s instanceof IStructuredSelection) {
			selection = (IStructuredSelection)s; 
		}
		
        if (selection != null && selection.getFirstElement() instanceof IFile)
        {
            final IFile currentFile = (IFile) selection.getFirstElement();
            ProgressMonitorDialog dialog = new ProgressMonitorDialog(Display.getDefault().getActiveShell());
            GenDocRunnable gendocRunnable = new GenDocRunnable(currentFile, null); 
            try
            {
                dialog.run(false, true, gendocRunnable);                
            }
            catch (InvocationTargetException e)
            {
                Activator.getDefault().getLog().log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
            }
            catch (InterruptedException e)
            {
                Activator.getDefault().getLog().log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
            }
            
        }		
        return null;
	}
}