/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.pptx;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.eclipse.gendoc.document.parser.documents.Document;
import org.eclipse.gendoc.document.parser.documents.DocumentFactory;
import org.eclipse.gendoc.document.parser.documents.Document.CONFIGURATION;

/**
 * A factory for creating XLSX objects.
 */
public class PPTXFactory implements DocumentFactory
{
    public PPTXFactory()
    {
    }

    public Document loadDocument(File documentFile)
    {
        return loadDocument(documentFile, null);
    }

    public Document loadDocument(File documentFile, Map<CONFIGURATION, Boolean> configuration)
    {
        try {
            return loadDocument(documentFile.toURI().toURL(), configuration);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null ;
    }

    public Document loadDocument(URL documentFile)
    {
        return loadDocument(documentFile, null);
    }

    public Document loadDocument(URL documentFile, Map<CONFIGURATION, Boolean> configuration)
    {
        return new PPTXDocument(documentFile,configuration);
    }

}
