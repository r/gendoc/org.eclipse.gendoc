/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.pptx;

import org.eclipse.gendoc.document.parser.documents.openoffice.OpenOfficeHelper;

public class PPTXHelper extends OpenOfficeHelper {
	
	public static final String GENDOC_FLAT_STRUCTURE_ELEM = "gendoc:flatSlide";
	public static final String GENDOC_REF_ELEM = "gendoc:ref";
	public static final String GENDOC_REF_ELEM_REF_ATTR = "ref";
	public static final String GENDOC_REF_ELEM_TYPE_ATTR = "type";
	public static enum GENDOC_REF_TYPE {
		startSlide,
		endSlide
	}
	public static final String GENDOC_XPATHREF_ATTR = "gendoc:ref";
	
	public static final String PRESENTATION_NAMESPACE = "http://schemas.openxmlformats.org/presentationml/2006/main";
	public static final String PRESENTATION_FILE_NAME = "presentation.xml";    
    public static final String PRESENTATION_RELS_FILE_NAME = "presentation.xml.rels";
    public static final String PRESENTATION_PATH = "/ppt/";
    public static final String PRESENTATION_REL_PATH = "/ppt/_rels/";
    public static final String PRESENTATION_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.presentationml.presentation.main+xml";
    public static final String PRESENTATION_TEMPLATE_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.presentationml.template.main+xml";
    public static final String PRESENTATION_SLIDE_SHOW_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml";
    public static final String PRESENTATION_RELATIONSHIP = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument";

    public static final String SLIDE_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.presentationml.slide+xml";
    public static final String SLIDE_NAMESPACE = "http://schemas.openxmlformats.org/presentationml/2006/main";
    public static final String SLIDE_RELATIONSHIP = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide";    	
    
	public static final String SLIDE_ID_LIST_NODE = "p:sldIdLst";
    public static final String SLIDE_ID_NODE = "p:sldId";
    public static final String SLIDE_ID_ATTR = "r:id";
    
    public static final String DRAWING_ML_TEXT_PARAGRAPH = "a:p";
    public static final String DRAWING_ML_TEXT_STRING ="a:t";
}
