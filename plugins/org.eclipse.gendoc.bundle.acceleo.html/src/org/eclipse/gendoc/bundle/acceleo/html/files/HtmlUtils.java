/*****************************************************************************
 * Copyright (c) 2018 Ericsson AB.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.bundle.acceleo.html.files;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class HtmlUtils {	
	public static void visit(Node node, IHtmlVisitor visitor) {
		if (node instanceof Document) {
			boolean visitChildren = visitor.visitDocument((Document)node);
			if (visitChildren)
				visitChildren(node, visitor);
			visitor.visitDocumentAfter((Document)node);
		} else if (node instanceof Element) {
			boolean visitChildren = visitor.visitElement((Element)node);
			if (visitChildren)
				visitChildren(node, visitor);
			visitor.visitElementAfter((Element)node);			
		} else if (node instanceof Text) {
			visitor.visitText((Text)node);
		} else {
			visitChildren(node,visitor);
		}
	}

	private static void visitChildren(Node node, IHtmlVisitor visitor) {
		NodeList nl = node.getChildNodes();
		for (int i=0; i<nl.getLength(); i++) {
			Node n = nl.item(i);
			visit(n, visitor);
		}
	}
}
